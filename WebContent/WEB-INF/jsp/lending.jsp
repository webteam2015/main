<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<%@include  file="includes/head.jsp" %>

<body>

	<%@include  file="includes/navbar.jsp" %>
    <div class="container lending-blok">
      <div class="page-header">
          <h1 class="h-inside color-main-h">Лендинг</h1>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg-7">
          <p class="h-inside">Это веб-страница, построенная определенным образом, основной задачей которой является сбор контактных данных целевой аудитории</p>
          <h3 class="h-inside margin-page-inside">Мы гарантируем Вам:</h3>
          <ul class="h-inside text-ul">
            <li class="garant">высокую скорость разработки</li>
            <li class="garant">небольшую стоимость</li>
            <li class="garant">легко восприимчивый вид сайта</li>
          </ul> 
          <h3 class="h-inside margin-page-inside">Вы можете заказать дополнительно:</h3>
          <ul class="h-inside text-ul">
            <li class="extra">поддержку созданного сайта</li>
            <li class="extra">дальнейшее продвижение</li>
            <li class="extra">адаптивность под различные устройства</li>
          </ul>            
        </div>
        <div class="col-xs-6 col-lg-5">
        <div class="prices price-text">
            <p class="price-mame">Стоимость</p>
              <h1>от 10000</h1>
            <p><a class="btn price-btn btn-lg" role="button" href="/order/">Заказать сейчас</a></p>
          </div>
        </div>
      </div>      
    </div>

    <%@include  file="includes/footer.jsp" %>
</body>
</html>