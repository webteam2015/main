<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@include  file="includes/head.jsp" %>
</head>
<body>
<%@include  file="includes/navbar.jsp" %>
	<div class="success-form fail-form">
		<p>Ваша заявка отклонена.</p>
		<p class="success-form-text fail-form-text"><img src="../image/smiley_sad.png" class="order_img"></p>
	</div>
	<%@include  file="includes/footer.jsp" %>
</body>
</html>