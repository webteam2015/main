<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<%@include  file="includes/head.jsp" %>
<style>
	h2 {
		color: red;
		font-size: 68pt;
		text-align: center;
	}
	h3 {
		text-align: center;
	}
</style>

</head>
<body>
	<%@include  file="includes/navbar.jsp" %>
	<div class="success-form">
		<p>Ваша заявка принята!</p>
		<p class="success-form-text"><img src="../image/smiley_happy.png" class="order_img"></p>
		<p class="success-form-text">Мы свяжемся с Вами в течение </p>
		<p class="success-form-text"> 3 часов.</p>
	</div>
	<%@include  file="includes/footer.jsp" %>
</body>
</html>