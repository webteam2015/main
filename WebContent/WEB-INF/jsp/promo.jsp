<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<%@include  file="includes/head.jsp" %>

<body>
	<%@include  file="includes/navbar.jsp" %>
     <div class="container promo-blok">
      <div class="page-header">
          <h1 class="h-inside color-main-h">Визитка</h1>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg-7">
          <p class="h-inside">Это небольшой сайт, состоящий из одной или нескольких веб-страницы</p>
          <h3 class="h-inside margin-page-inside">Мы гарантируем Вам:</h3>
          <ul class="h-inside  ">
            <li class="garant">высокую скорость разработки</li>
            <li class="garant">небольшую стоимость</li>
            <li class="garant">дизайн по вашим требованиям</li>
          </ul> 
          <h3 class="h-inside margin-page-inside">Вы можете заказать дополнительно:</h3>
          <ul class="h-inside">
            <li class="extra">создание админки для Вашего личного управления сайтом</li>
            <li class="extra">дальнейшее продвижение</li>
            <li class="extra">поддержку созданного сайта</li>            
            <li class="extra">адаптивность под различные устройства</li>
          </ul>            
        </div>
        <div class="col-xs-6 col-lg-5">
        <div class="prices price-text">
            <p class="price-mame">Стоимость</p>
              <h1>от 5000</h1>
            <p><a class="btn btn-lg price-btn" role="button" href="/order/">Заказать сейчас</a></p>
          </div>
        </div>
      </div>      
    </div> 
    </div>   

   	<%@include  file="includes/footer.jsp" %>
</body>
</html>