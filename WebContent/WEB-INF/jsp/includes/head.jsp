<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache">
    <title>NetCode</title>
    <link rel="icon" type="image/vnd.microsoft.icon" href="/image/favicon.ico">
    <link type="text/css" href="/style/bootstrap.min.css" rel="stylesheet" />
    <link type="text/css" href="/style/bootstrap-theme.min.css" rel="stylesheet" />
    <link type="text/css" href="/style/style.css" rel="stylesheet" />
</head>