<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="navbar navbar-fixed-top navbar-inverse navbar-shadow" role="navigation">
    
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="brand-title" href="/home/"><img class="navbar-logo" src="../image/logo.png"></a>
        </div>
        <div class="collapse navbar-collapse pull-right">
            <ul class="nav navbar-nav">
                <li class="border-menu-element"><a class="nav-link" href="/promo/">Визитка</a></li>
                <li class="border-menu-element"><a class="nav-link" href="/lending/">Лендинг</a></li>
                <li class="border-menu-element"><a class="nav-link" href="/personal/">Личный сайт</a></li>
                <li class="border-menu-element"><a class="nav-link" href="/corporate/">Корпоративный сайт</a></li>
                <li class="border-menu-element"><a class="nav-link" href="/marketplace/">Интернет магазин</a></li>
                <li class="border-menu-element"><a class="nav-link" href="/order/" >Заказать сейчас</a></li>
            </ul>
        </div>        
    
</div>


