﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<%@include  file="includes/head.jsp" %>

<body>
	<%@include  file="includes/navbar.jsp" %>
    <h1 class="main-header">Компания NetCode предлагает Вам:</h1>
    <div class="home-main-contaner">
        <section class="home-main-content">
            <article class="home-text-and-img section_element-left">
                <header>
                    <h2 class="home-h-inside">Необычный интерфейс</h2>
                </header>
                <p class="home-p-inside">Сейчас в сети интернет появляется всё больше сайтов с приятным современным интерфейсом.
                    Но для того чтобы выделиться, нужно больше чем просто хороший внешний вид сайта.
                    Нужно чтобы запомнили именно Вас. Мы поможем Вам добиться этого.</p>
            </article>
            <img class="home-main-imgs section_element-right" src="/image/1.jpg">
        </section>
        <section class="home-main-content">
            <article class="home-text-and-img section_element-right">
                <header>
                    <h2 class="home-h-inside">Ориентированность на пользователя</h2>
                </header>
                <p class="home-p-inside">Когда Вы заходите на сайт и видите сайт, с небольшим количеством информации
                 и приятные цвета, то возникает желание остаться на этом сайте подольше.
                 Именно поэтому, мы учитываем визуальные предпочтения людей с психологической точки зрения.</p>
            </article>
            <img class="home-main-imgs-left home-main-imgs section_element-left" src="/image/2.jpg">
        </section>
        <section class="home-main-content">
            <article class="home-text-and-img section_element-left">
                <header>
                    <h2 class="home-h-inside">Адаптивность</h2>
                </header>
                <p class="home-p-inside">В век, когда у каждого человека по два-три технических устройства, необходимость адаптивности
                    сайтов стала важнейшей задачей при веб-разработке.
                    Адаптивность нужна для того, чтобы было удобно просматривать сайты с устройств различных разрешений и форматов.</p>
            </article>
            <img class="home-main-imgs section_element-right" src="/image/3.jpg">
        </section>
        <section class="home-main-content">
            <article class="home-text-and-img section_element-right">
                <header>
                    <h2 class="home-h-inside">Поддержка</h2>
                </header>
                <p class="home-p-inside">При необходимости компания NetCode готова предоставить поддержку любого рода, после сдачи сайта в эксплуатацию.
                    В поддержку входят такие элементы как: наполнение актуальным контентом, продвижение сайта и реклама.</p>
            </article>
            <img class="home-main-imgs-left home-main-imgs section_element-left" src="/image/4.jpg">
        </section>
        <section class="home-main-content home-after-main-content">
            <article class="home-text-and-img section_element-left">
                <header>
                    <h2 class="home-h-inside">Надёжность</h2>
                </header>
                <p class="home-p-inside">При выборе компании Вы можете обратиться
                к мнению знакомых, почитать отзывы в интернете, выбрать
                услуги компании, сайт который размещён на
                первой строчке гугла и т.д.. Но Вы можете судить
                о надёжности, только когда сами воспользуетесь услугой!</p>
            </article>
            <img class="home-main-imgs section_element-right" src="/image/5.jpg">
        </section>
        <!--<div class="home-block-actions">
            <div class="home-actions-button home-actions-first-last">
                <a href=""><img src="/image/back_left.png"></a>
            </div>
            <div class="home-actions home-action-first-second home-actions-centre"></div>
            <div class="home-actions home-action-first-second home-actions-centre"></div>
            <div class="home-actions home-actions-centre"></div>
            <div class="home-actions-button home-actions-first-last">
                <a href="" class="home-actions-right-button"><img src="/image/back_right.png"></a>
            </div>
        </div>-->
    </div>         

    <script src="/script/blocks.js"></script>
    <%@include  file="includes/footer.jsp" %>
</body>

</html>