<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<%@include  file="includes/head.jsp" %>

</head>
<body>
	<%@include  file="includes/navbar.jsp" %>
	
		
	<h1 class="order-h1"> Заполните форму для заказа сайта </h1>			
		<div class="order-form">
		<form class="form-horizontal" role="form" action="" method="post">
		  <div class="form-group">
		    <label for="inputName" class="col-sm-4 control-label order-form-text">Имя:</label>
		    <div class="col-sm-8 order-form-element">
		      <input type="text" name="name" class="form-control" id="inputName" required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputPhone" class="col-sm-4 control-label order-form-text">Телефон:</label>
		    <div class="col-sm-8 order-form-element">
		      <input type="text" name="phone" class="form-control" id="inputPhone" required>
		    </div>
		  </div>
		   <div class="form-group">
		   <label for="inputType" class="col-sm-4 control-label order-form-text">Тип сайта:</label>
		   <div class="col-sm-8 order-form-element">
		  <select name="type" class="form-control" id="inputType">
			  <option>Визитка</option>
			  <option>Лендинг</option>
		  </select>
		  </div>
		  </div>
		  <div class="form-group">
		  <div class="col-sm-offset-4 col-sm-8">
		      <button type="submit" class="btn order-form-button" value="Заказать">Заказать</button>
		  </div>
		  </div>
		</form>
		</div>

		
	
	<%@include  file="includes/footer.jsp" %>
</body>
</html>