/**
 * Created by takayuki on 18.10.15.
 */
window.addEventListener("load", function () {
    "use strict";

    var blocks = document.getElementsByClassName("home-main-content"),
        hiddenBlocks = [],
        visibleBlocks = [],
        visibleCount = 0;

    function showElement() {
        var element = hiddenBlocks.pop();
        if (element) {
            element.classList.add("home-main-content-visible");
            visibleBlocks.push(element);
            visibleCount += 1;
        }
    }

    function hideElement() {
        var element = visibleBlocks.pop();
        if (element) {
            element.classList.remove("home-main-content-visible");
            hiddenBlocks.push(element);
            visibleCount -= 1;
        }
    }

    function showElements() {
        var scrolled = window.pageYOffset || document.documentElement.scrollTop,
            count = ((scrolled + 450) / 500) - visibleCount,
            i;

        for (i = 0; i < count; i += 1) {
            showElement();
        }
    }

    Array.prototype.forEach.call(blocks, function (element) {
        hiddenBlocks.unshift(element);
    });
    showElements();

    window.addEventListener("scroll", function () {
        var scrolled = window.pageYOffset || document.documentElement.scrollTop;

        if (Math.floor((scrolled + 450) / 500) > visibleCount - 1) {
            showElement();
        } else if (Math.floor((scrolled + 450) / 500) < visibleCount - 1) {
            hideElement();
        }
    }, false);
}, false);

