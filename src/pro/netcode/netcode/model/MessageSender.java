package pro.netcode.netcode.model;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Класс отвечающий за отправку сообщений.
 * 
 * @author Прокофьев Василий
 * 
 */
public class MessageSender {
	/**
	 * Создание JavaMail сессии.
	 *
	 * @param smtpHost
	 *            smtp хост.
	 * @param smtpPort
	 *            smtp порт.
	 * @param username
	 *            username e-mail аккаунт.
	 * @param password
	 *            пароль e-mail аккаунта.
	 * @return созданная сессия.
	 */
	public Session createSession(String smtpHost, int smtpPort,
			String username, String password) {
		Properties props = new Properties();
		props.put("mail.smtp.port", smtpPort);
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		return Session.getDefaultInstance(props,
				createAuthenticator(username, password));
	}

	/**
	 * Создание MIME сообщения.
	 *
	 * @param session
	 *            JavaMail сессия.
	 * @param subject
	 *            тема сообщения.
	 * @param from
	 *            отправитель сообщения.
	 * @param to
	 *            получатель сообщения.
	 * @param recipientType
	 *            тип сообщения.
	 * @return {@code MimeMessage}.
	 * @throws MessagingException
	 *             если возникла какая либо ошибка.
	 */
	public MimeMessage createMimeMessage(Session session, String subject,
			String from, String to, Message.RecipientType recipientType)
			throws MessagingException {
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(from));
		msg.setRecipients(recipientType, InternetAddress.parse(to));
		msg.setSubject(subject);
		msg.setContent(new MimeMultipart());
		return msg;
	}

	/**
	 * Добавление текста в сообщение.
	 *
	 * @param message
	 *            сообщение.
	 * @param text
	 *            текст сообщения.
	 * @param charset
	 *            кодировка.
	 * @param type
	 *            тип сообщения (html, plain ...).
	 * @return сообщение с текстом,
	 * @throws IOException
	 *             если произошла ошибка ввода/вывода.
	 * @throws MessagingException
	 *             если возникла какая либо ошибка.
	 */
	public MimeMessage addText(MimeMessage message, String text,
			String charset, String type) throws IOException, MessagingException {
		MimeBodyPart textPart = new MimeBodyPart();
		textPart.setText(text, charset, type);
		MimeMultipart multipart = (MimeMultipart) message.getContent();
		multipart.addBodyPart(textPart);
		return message;
	}

	/**
	 * Добавление вложения к сообщению.
	 *
	 * @param message
	 *            сообщение.
	 * @param file
	 *            файл для вложения.
	 * @return сообщение с вложением.
	 * @throws IOException
	 *             если произошла ошибка ввода/вывода.
	 * @throws MessagingException
	 *             если возникла какая либо ошибка.
	 */
	public MimeMessage addAttachment(MimeMessage message, File file)
			throws IOException, MessagingException {
		MimeBodyPart filePart = new MimeBodyPart();
		filePart.attachFile(file);
		MimeMultipart multipart = (MimeMultipart) message.getContent();
		multipart.addBodyPart(filePart);
		return message;
	}

	/**
	 * Отправка MIME сообщения.
	 *
	 * @param message
	 *            сообщение.
	 * @throws MessagingException
	 *             если возникла какая либо ошибка.
	 */

	public void sendMimeMessage(MimeMessage message) throws MessagingException {
		Transport.send(message);
	}

	private Authenticator createAuthenticator(final String username,
			final String password) {
		return new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		};
	}
}
