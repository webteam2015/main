package pro.netcode.netcode.model;

import pro.netcode.netcode.controller.managers.MailConfigManager;
import pro.netcode.netcode.controller.properties.MailProperties;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

public class Message implements Runnable {
	public static final String ENCODING = "utf-8";
	public static final String MESSAGE_TYPE = "plain";
	private MailConfigManager configManager;
	private String title;
	private Map<String, String> params;

	public Message(String title, Map<String, String> params) {
		configManager = MailConfigManager.getInstance();
		this.title = title;
		this.params = params;
	}

	/**
	 * Метод для отправки соообщения
	 */
	public void send() {
		Thread t = new Thread(this);
		t.start();
	}
	
	@Override
	public void run() {
		try {
			sendMessage();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void sendMessage() throws MessagingException, IOException {
		String smtpHost = configManager.getString(MailProperties.SMTP_HOST); // host of smtp mail server.
		int smtpPort = configManager.getInt(MailProperties.SMTP_PORT); // port of smtp mail server
		String username = configManager.getString(MailProperties.EMAIL_ACCOUNT); // your username e-mail account
		String password = configManager.getString(MailProperties.EMAIL_PASSWORD); // your password e-mail account
		String addressFrom = configManager.getString(MailProperties.ADDRESS_FROM);
		String addressTo = configManager.getString(MailProperties.ADDRESS_TO);

		MessageSender messageSender = new MessageSender();

		// Создание новой JavaMail сессии.
		Session session = messageSender.createSession(smtpHost, smtpPort,
				username, password);
		// Создание пустого сообщния.
		MimeMessage message = messageSender.createMimeMessage(session, title,
				addressFrom, addressTo,
				javax.mail.Message.RecipientType.TO);
		// Добавление текста сообщения.
		//messageSender.addText(message, createText(), encoding, type);
		messageSender.addText(message, createText(), ENCODING, MESSAGE_TYPE);
		// Отправка сообщения
		messageSender.sendMimeMessage(message);
	}

	private String createText() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Новая заявка\n");
		for(String currentParamName : params.keySet()) {
			buffer.append(currentParamName.toString());
			buffer.append(": ");
			buffer.append(params.get(currentParamName));
			buffer.append("\n");
		}
		return buffer.toString();
	}
}
