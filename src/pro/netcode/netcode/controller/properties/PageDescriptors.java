package pro.netcode.netcode.controller.properties;

public enum PageDescriptors {
	HOME,
	PROMO,
	LENDING,
	PERSONAL,
	CORPORATE,
	MARKETPLACE,
	ORDER,
	SUCCESS,
	FAIL,
	NOT_FOUND
}
