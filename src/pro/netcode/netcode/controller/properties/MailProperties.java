package pro.netcode.netcode.controller.properties;

public enum MailProperties {
    SMTP_HOST,
    SMTP_PORT,
    EMAIL_ACCOUNT,
    EMAIL_PASSWORD,
    ADDRESS_FROM,
    ADDRESS_TO;
}
