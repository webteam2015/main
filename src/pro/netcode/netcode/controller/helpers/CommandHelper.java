package pro.netcode.netcode.controller.helpers;

import pro.netcode.netcode.controller.properties.RequestProperties;

public class CommandHelper {
	private RequestProperties requestType;

	public CommandHelper(RequestProperties requestType) {
		this.requestType = requestType;
	}

	public RequestProperties getRequestType() {
		return requestType;
	}
}
