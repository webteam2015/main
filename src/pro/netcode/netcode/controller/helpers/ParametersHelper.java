package pro.netcode.netcode.controller.helpers;

import javax.servlet.http.HttpServletRequest;

public class ParametersHelper {
    private HttpServletRequest request;

    public ParametersHelper(HttpServletRequest request) {
        this.request = request;
    }

    public String getParameter(String name) {
        return request.getParameter(name);
    }
}
