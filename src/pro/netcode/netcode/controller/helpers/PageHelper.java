package pro.netcode.netcode.controller.helpers;

import javax.servlet.http.HttpServletRequest;

import pro.netcode.netcode.controller.properties.PageDescriptors;
import pro.netcode.netcode.controller.managers.PageManager;

public class PageHelper{
	private HttpServletRequest request;

	public PageHelper(HttpServletRequest request) {
		this.request = request;
	}

	public PageDescriptors getPage() {
		PageManager manager = PageManager.getInstance();
		return manager.getKey(request.getServletPath());
	}
}
