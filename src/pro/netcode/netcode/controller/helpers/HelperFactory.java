package pro.netcode.netcode.controller.helpers;

import javax.servlet.http.HttpServletRequest;

import pro.netcode.netcode.controller.properties.RequestProperties;

public class HelperFactory{
	private HttpServletRequest request;
	private RequestProperties requestType;

	public HelperFactory(HttpServletRequest request, RequestProperties requestType) {
		this.request = request;
		this.requestType = requestType;
	}

	public PageHelper getPageHelper() {
		return new PageHelper(request);
	}	
	
	public CommandHelper getCommandHelper() {
		return new CommandHelper(requestType);
	}

	public ParametersHelper getParametersHelper() {
		return new ParametersHelper(request);
	}
}
