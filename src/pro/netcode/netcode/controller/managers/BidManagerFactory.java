package pro.netcode.netcode.controller.managers;

import pro.netcode.netcode.controller.properties.BidTypes;

import java.util.HashMap;
import java.util.Map;


public class BidManagerFactory {
    private static BidManagerFactory instance;
    private Map<BidTypes, BidManager> bidManagerMap;

    private BidManagerFactory() {
        bidManagerMap = new HashMap<>(BidTypes.values().length);
    }

    public BidManager getBidManager(BidTypes type) {
        if(!bidManagerMap.containsKey(type)) {
            synchronized (this) {
                if (!bidManagerMap.containsKey(type)) {
                    bidManagerMap.put(type, new BidManager(type.toString().toLowerCase()));
                }
            }
        }
        return bidManagerMap.get(type);
    }

    public static BidManagerFactory getInstance() {
        if(instance == null) {
            synchronized (BidManagerFactory.class) {
                if (instance == null) {
                    instance = new BidManagerFactory();
                }
            }
        }
        return instance;
    }
}
