package pro.netcode.netcode.controller.managers;

import java.util.*;

public class BidManager {
    private Map<String, String> resourse;

    BidManager(String boundleName) {
        ResourceBundle resourseBundle = ResourceBundle.getBundle(boundleName);
        Enumeration<String> keys = resourseBundle.getKeys();
        resourse = new HashMap<>();
        while(keys.hasMoreElements()) {
            String currentKey = keys.nextElement();
            String currentValue = resourseBundle.getString(currentKey);
            resourse.put(currentKey, currentValue);
        }
        resourse = Collections.unmodifiableMap(resourse);
    }

    public String getValue(String key) {
        return resourse.get(key);
    }

    public Map<String, String> getMap() {
        return resourse;
    }
}
