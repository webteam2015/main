package pro.netcode.netcode.controller.managers;

import pro.netcode.netcode.controller.properties.MailProperties;

import java.util.ResourceBundle;

public class MailConfigManager {
	private static MailConfigManager instance;
	private static final String FILE_NAME = "mail";
	private ResourceBundle prop;

	private MailConfigManager() {
		prop = ResourceBundle.getBundle(FILE_NAME);
	}

	public String getString(MailProperties property) {
		return prop.getString(property.toString());
	}

	public int getInt(MailProperties property) {
		try {
			return Integer.parseInt(prop.getString(property.toString()));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static MailConfigManager getInstance() {
		if (instance == null) {
			synchronized (MailConfigManager.class) {
				if (instance == null) {
					instance = new MailConfigManager();
				}
			}
		}
		return instance;
	}
}