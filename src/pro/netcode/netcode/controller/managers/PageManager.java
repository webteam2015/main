package pro.netcode.netcode.controller.managers;

import java.util.Enumeration;
import java.util.ResourceBundle;

import pro.netcode.netcode.controller.properties.PageDescriptors;

public class PageManager {
	private static PageManager instance;
	private static final String BOUNDLE_NAME = "pages";
	private ResourceBundle resourse;
	
	private PageManager() {
		resourse = ResourceBundle.getBundle(BOUNDLE_NAME);
	}
	
	public String getValue(PageDescriptors key) {
		return (String) resourse.getObject(key.toString());
	}
	
	public PageDescriptors getKey(String value) {
		PageDescriptors result = PageDescriptors.NOT_FOUND;
		Enumeration<String> keys = resourse.getKeys();
		while(keys.hasMoreElements()) {
			String currentKey = keys.nextElement();
			String currentValue = resourse.getString(currentKey);
			if (currentValue.equals(value)) {
				result = PageDescriptors.valueOf(currentKey);
				break;
			}
		}
		return result;
	}
	
	public static PageManager getInstance() {
		if(instance == null) {
			synchronized (PageManager.class) {
				if (instance == null) {
					instance = new PageManager();
				}
			}
		}
		return instance;
	}
}
