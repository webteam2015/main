package pro.netcode.netcode.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pro.netcode.netcode.controller.commands.Command;
import pro.netcode.netcode.controller.commands.CommandFactory;
import pro.netcode.netcode.controller.helpers.HelperFactory;
import pro.netcode.netcode.controller.properties.PageDescriptors;
import pro.netcode.netcode.controller.properties.RequestProperties;

/**
 * Servlet implementation class Controller
 */
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response, RequestProperties.GET);
		
		
		//HelperFactory helperFactory = new HelperFactory(request);
		//PageHelper pageHelper = helperFactory.getPageHelper();
		//Dispatcher disp = Dispatcher.getInstance();
		//disp.dispatch(this, request, response, pageHelper.getPage());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response, RequestProperties.POST);
	}
	
	private void processRequest(HttpServletRequest request,
			HttpServletResponse response, RequestProperties requestType) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		HelperFactory helperFactory = new HelperFactory(request, requestType);
		CommandFactory commandFactory = CommandFactory.getInstance();
		Command command = commandFactory.getCommand(helperFactory);
		PageDescriptors page = command.execute();
		Dispatcher disp = Dispatcher.getInstance();
		disp.dispatch(this, request, response, page);
		
		
	}
}
