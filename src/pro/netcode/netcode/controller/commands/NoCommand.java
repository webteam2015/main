package pro.netcode.netcode.controller.commands;

import pro.netcode.netcode.controller.properties.PageDescriptors;

public class NoCommand implements Command {

	@Override
	public PageDescriptors execute() {
		return PageDescriptors.NOT_FOUND;
	}

}
