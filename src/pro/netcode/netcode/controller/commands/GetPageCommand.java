package pro.netcode.netcode.controller.commands;

import pro.netcode.netcode.controller.properties.PageDescriptors;
import pro.netcode.netcode.controller.helpers.HelperFactory;
import pro.netcode.netcode.controller.helpers.PageHelper;

public class GetPageCommand implements Command {
	private HelperFactory helperFactory;

	public GetPageCommand(HelperFactory helperFactory) {
		this.helperFactory = helperFactory;
	}

	@Override
	public PageDescriptors execute() {
		PageHelper pageHelper = helperFactory.getPageHelper();
		return pageHelper.getPage();
	}

}
