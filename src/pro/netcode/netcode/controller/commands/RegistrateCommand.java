package pro.netcode.netcode.controller.commands;

import pro.netcode.netcode.controller.properties.BidTypes;
import pro.netcode.netcode.controller.properties.PageDescriptors;
import pro.netcode.netcode.controller.helpers.HelperFactory;
import pro.netcode.netcode.controller.helpers.PageHelper;
import pro.netcode.netcode.controller.helpers.ParametersHelper;
import pro.netcode.netcode.controller.managers.BidManager;
import pro.netcode.netcode.controller.managers.BidManagerFactory;
import pro.netcode.netcode.model.Message;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class RegistrateCommand implements Command {
	public static final String BITRIX_URL = "https://akordeon.bitrix24.ru/crm/configs/import/lead.php";
	public static final String BITRIX_LOGIN = "Takayuki.sempai@gmail.com";
	public static final String BITRIX_PASSWORD = "Takayuki.sempai";
	private HelperFactory helperFactory;
	private String MESSAGE_TITLE = "Форма регистрации";

	public RegistrateCommand(HelperFactory helperFactory) {
		this.helperFactory = helperFactory;
	}

	@Override
	public PageDescriptors execute() {
		Map<String, String> params = getParams();
		if(checkParams(params)) {
			sendToBitrix(params);
			sendMessage(params);
			return choosePage(true);
		} else {
			return choosePage(false);
		}
	}

	private Map<String, String> getParams() {
		ParametersHelper parametersHelper = helperFactory.getParametersHelper();
		BidManager bidManager = BidManagerFactory.getInstance().getBidManager(BidTypes.REGISTRATE);
		Map<String, String> types = bidManager.getMap();
		Map<String, String> params = new HashMap<>(types.keySet().size());
		for(String currentKey : types.keySet()) {
			String currentType = types.get(currentKey);
			String currentValue = parametersHelper.getParameter(currentType);
			params.put(currentKey, currentValue);
		}
		return params;
	}

	private boolean checkParams(Map<String, String> params) {
		//TODO Сделать проверку параметров
		return true;
	}

	private void sendToBitrix(Map<String, String> params) {
		try {
			String urlParameters = "LOGIN=" + URLEncoder.encode(BITRIX_LOGIN, "UTF-8") +
                    "&PASSWORD=" + URLEncoder.encode(BITRIX_PASSWORD, "UTF-8") +
                    "&TITLE=" + URLEncoder.encode(params.get("TYPE"), "UTF-8") +
					"&PHONE_MOBILE=" + URLEncoder.encode(params.get("PHONE"), "UTF-8") +
					"&NAME=" + URLEncoder.encode(params.get("NAME"), "UTF-8");
			//TODO Переписать генерацию параметров
			sendBitrixRequest(urlParameters);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	private void sendBitrixRequest(String urlParameters) {
		//TODO почистить. Выделаить в отдельный класс
		URL url;
		HttpURLConnection connection = null;
		try {
			//Create connection
			url = new URL(BITRIX_URL);
			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");

			connection.setRequestProperty("Content-Length", "" +
					Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");

			connection.setUseCaches (false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			//Send request
			DataOutputStream wr = new DataOutputStream (
					connection.getOutputStream ());
			wr.writeBytes (urlParameters);
			wr.flush ();
			wr.close ();

			//Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer response = new StringBuffer();
			while((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			//TODO Написать проверку на добавление лида
			//return response.toString();

		} catch (Exception e) {

			e.printStackTrace();
			//return null;

		} finally {

			if(connection != null) {
				connection.disconnect();
			}
		}
	}

	private void sendMessage(Map<String, String> params) {
		Message message = new Message(MESSAGE_TITLE, params);
		message.send();
	}

	private PageDescriptors choosePage(boolean isSuccess) {
		PageHelper pageHelper = helperFactory.getPageHelper();
		PageDescriptors page = PageDescriptors.NOT_FOUND;
		if(pageHelper.getPage() == PageDescriptors.ORDER) {
			page = isSuccess ? PageDescriptors.SUCCESS : PageDescriptors.FAIL;
		}
		return page;
	}
}
