package pro.netcode.netcode.controller.commands;

import pro.netcode.netcode.controller.properties.RequestProperties;
import pro.netcode.netcode.controller.helpers.CommandHelper;
import pro.netcode.netcode.controller.helpers.HelperFactory;

public class CommandFactory {
	private static CommandFactory instance;
	
	public Command getCommand(HelperFactory helperFactory) {
		CommandHelper helper = helperFactory.getCommandHelper();
		RequestProperties requestType = helper.getRequestType();
		switch (requestType) {
		case GET:
			return new GetPageCommand(helperFactory);
		case POST:
			return new RegistrateCommand(helperFactory);
		default:
			return new NoCommand();
		}
		
	}
	
	public static CommandFactory getInstance() {
		if(instance == null) {
			synchronized (CommandFactory.class) {
				if(instance == null) {
					instance = new CommandFactory();
				}
			}
		}
		return instance;
	}
}
