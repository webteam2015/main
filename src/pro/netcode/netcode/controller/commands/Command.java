package pro.netcode.netcode.controller.commands;

import pro.netcode.netcode.controller.properties.PageDescriptors;

public interface Command {
	PageDescriptors execute();
}
