package pro.netcode.netcode.controller;

import pro.netcode.netcode.controller.properties.PageDescriptors;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Dispatcher {
	private static Dispatcher instance;

	public void dispatch(Controller controller, HttpServletRequest request, HttpServletResponse response, PageDescriptors page) {
		forvard(controller, request, response, page);
	}
	
	private void forvard(Controller controller, HttpServletRequest request, HttpServletResponse response, PageDescriptors page) {
		try {
			switch(page) {
			case HOME: controller.getServletContext().getNamedDispatcher("Home").forward(request,response);
				break;
			case PROMO: controller.getServletContext().getNamedDispatcher("Promo").forward(request,response);
				break;
			case LENDING: controller.getServletContext().getNamedDispatcher("Lending").forward(request,response);
				break;
			case PERSONAL: controller.getServletContext().getNamedDispatcher("Personal").forward(request,response);
				break;
			case CORPORATE: controller.getServletContext().getNamedDispatcher("Corporate").forward(request,response);
				break;
			case MARKETPLACE: controller.getServletContext().getNamedDispatcher("Marketplace").forward(request,response);
				break;
			case ORDER: controller.getServletContext().getNamedDispatcher("Order").forward(request,response);
				break;
			case SUCCESS: controller.getServletContext().getNamedDispatcher("Success").forward(request,response);
				break;
			case FAIL: controller.getServletContext().getNamedDispatcher("Fail").forward(request,response);
					break;
			default: response.sendError(404);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Dispatcher getInstance() {
		if(instance == null) {
			synchronized (Dispatcher.class) {
				if(instance == null) {
					instance = new Dispatcher();
				}
			}
		}
		return instance;
	}
}
